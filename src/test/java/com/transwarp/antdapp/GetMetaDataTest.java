package com.transwarp.antdapp;

import com.transwarp.antdapp.repository.ElasticRepository;
import com.transwarp.antdapp.repository.HbaseRepository;
import com.transwarp.antdapp.repository.es.bean.IpInfo;
import com.transwarp.antdapp.repository.inceptor.InceptorRepository;
import com.transwarp.antdapp.repository.inceptor.TableDetailRepository;
import com.transwarp.antdapp.repository.inceptor.bean.TableInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;

@Slf4j
@SpringBootTest
public class GetMetaDataTest {
    @Autowired
    private InceptorRepository inceptorDao;
    @Autowired
    private ElasticRepository elasticDao;
    @Autowired
    private HbaseRepository hbaseRepository;
    @Autowired
    private TableDetailRepository inceptorTableDetailRepository;
    @Test
    public void getMetaDataTest() {
        List<TableInfo> metaData = inceptorDao.getMetaData();
        assert !metaData.isEmpty();
    }

    @Test
    public void elasticsearchTest() {
        String esInfo = elasticDao.getEsInfo();
        log.info(esInfo);
        assert esInfo != null;
    }

    @Test
    public void elasticsearchIndexTest() {
        Integer esInfo = elasticDao.getIndexCount();
        assert esInfo != null;
    }

    @Test
    public void hbaseTest() {
        List<String> tableNames = hbaseRepository.getTableNames();
        assert !tableNames.isEmpty();
    }
    @Test
    public void getInceptorTableDetail(){
        Sort sort = Sort.by(Sort.Direction.DESC, "tableId");
        PageRequest pageable = PageRequest.of(0, 5).withSort(sort);
        Page<TableInfo> all = inceptorTableDetailRepository.findAll(pageable);
        assert !all.isEmpty();
    }
    @Test
    public void getHbaseTableMetaData(){
        hbaseRepository.getTableInfoDetails();
    }
    @Test
    public void getEsIndexContent(){
        elasticDao.searchNews("poc.news_analyze_zh",0,5);
    }
    @Test
    public void getEsIpInfo(){
        Page<IpInfo> ip_search = elasticDao.searchIpInfo(null,null,null,null,"ip_search", 0, 5);
        assert !ip_search.isEmpty();
    }
}
