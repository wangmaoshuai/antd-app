package com.transwarp.antdapp.controller;

import com.alibaba.fastjson.JSONArray;
import com.transwarp.antdapp.entity.common.Iterm;
import com.transwarp.antdapp.entity.common.Response;
import com.transwarp.antdapp.entity.dashboard.Offline;
import com.transwarp.antdapp.entity.dashboard.ProportionSales;
import com.transwarp.antdapp.entity.dashboard.Word;
import com.transwarp.antdapp.repository.ElasticRepository;
import com.transwarp.antdapp.repository.es.bean.EsIndexInfo;
import com.transwarp.antdapp.repository.es.bean.IpInfo;
import com.transwarp.antdapp.repository.es.bean.NewsInfo;
import com.transwarp.antdapp.repository.hbase.bean.HbaseTableInfo;
import com.transwarp.antdapp.service.DashboardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.function.Function;

/**
 * @author wangm
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class DashboardController {
    private final DashboardService dashboardService;
    private final ElasticRepository elasticRepository;
    private static final Map<String, Function<String, Integer>> RESOURCE_USAGE = Collections.unmodifiableMap(
            new HashMap<String, Function<String, Integer>>(2) {{
                put("hbase", (source) -> {
                    Double random = (Math.random() * 1000);
                    return random.intValue();
                });
                put("default", (source) -> {
                    Double random = (Math.random() * 1000);
                    return random.intValue();
                });
            }}
    );

    @GetMapping("/sales")
    public Response<JSONArray> getSales(@RequestParam("type") String type) {
        return new Response<JSONArray>()
                .setData(ProportionSales.getMockData(type))
                .setSuccess(true);
    }

    @GetMapping("/active")
    public Response<JSONArray> getActive() {
        int hours = 24;
        List<Object> result = new ArrayList<>(24);
        for (int i = 0; i <= hours; i++) {
            String hour = String.format("%02d:00", i);
            double count = Math.floor((Math.random() * 200) + i * 50);
            Iterm<String, Double> item = new Iterm<String, Double>().setX(hour).setY(count);
            result.add(item);
        }
        return new Response<JSONArray>().setData(new JSONArray(result)).setSuccess(true);
    }

    @GetMapping("/getResourceUseData")
    public Response<Integer> getResourceUseData(@RequestParam("sourceType") String sourceType) {
        return new Response<Integer>().setSuccess(true)
                .setData(RESOURCE_USAGE.getOrDefault(sourceType, RESOURCE_USAGE.get("default"))
                        .apply(sourceType));
    }

    @GetMapping("/getOfflineData")
    public Response<Offline> getOfflineData() {
        return new Response<Offline>().setSuccess(true)
                .setData(Offline.getData());
    }

    @GetMapping("/getWordData")
    public Response<List<Word.MetaData>> getWordData() {
        return new Response<List<Word.MetaData>>().setSuccess(true)
                .setData(Word.getData());
    }

    @GetMapping("/getIndexCount")
    public Response<List<Iterm<String, Integer>>> getIndexCount(@RequestParam("type") String type) {
        return new Response<List<Iterm<String, Integer>>>()
                .setData(dashboardService.getIndexCount(type))
                .setSuccess(true);
    }

    @GetMapping("/getIndexDetails")
    public Response<List<EsIndexInfo>> getIndexDetails() {
        return new Response<List<EsIndexInfo>>()
                .setData(dashboardService.getIndexDetails())
                .setSuccess(true);
    }

    @GetMapping("/getHbaseTableDetails")
    public Response<List<HbaseTableInfo>> getHbaseTableDetails() {
        return new Response<List<HbaseTableInfo>>()
                .setData(dashboardService.getHbaseTableDetails())
                .setSuccess(true);
    }

    @GetMapping("/getIndexSource")
    public Response<List<NewsInfo>> getIndexSource(@RequestParam("index") String index,
                                                   @RequestParam(name = "searchKey", defaultValue = "") String searchKey,
                                                   @RequestParam(name = "from", defaultValue = "0") Integer from,
                                                   @RequestParam(name = "size", defaultValue = "5") Integer size) {

        List<NewsInfo> search;
        if (StringUtils.isEmpty(searchKey)) {
            search = elasticRepository.searchNews(index, from, size);
        } else {
            search = elasticRepository.searchNews(searchKey, index, from, size);
        }
        return new Response<List<NewsInfo>>().setSuccess(true).setData(search);
    }

    @GetMapping("/getIpInfo")
    public Response<Page<IpInfo>> getIpInfo(
            @RequestParam(name = "endTime",required = false) String endTime,
            @RequestParam(name = "startTime",required = false) String startTime,
            @RequestParam(name = "status",required = false) String status,
            @RequestParam(name = "ip",required = false) String ip,
            @RequestParam(value = "index", defaultValue = "ip_search") String index,
            @RequestParam(name = "current", defaultValue = "0") Integer current,
            @RequestParam(name = "pageSize", defaultValue = "5") Integer size) {
        Page<IpInfo> ipInfos = elasticRepository.searchIpInfo(ip, status, startTime, endTime, index, current-1, size);
        return new Response<Page<IpInfo>>().setSuccess(true).setData(ipInfos);
    }
}
