package com.transwarp.antdapp.controller.inceptor;

import com.transwarp.antdapp.entity.common.Iterm;
import com.transwarp.antdapp.entity.common.Response;
import com.transwarp.antdapp.repository.inceptor.bean.TableInfo;
import com.transwarp.antdapp.repository.inceptor.bean.ViewInfo;
import com.transwarp.antdapp.service.InceptorDashboardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author wangm
 */
@Slf4j
@RestController
public class InceptorController {
    private final InceptorDashboardService inceptorDashboardService;

    public InceptorController(InceptorDashboardService inceptorDashboardService) {
        this.inceptorDashboardService = inceptorDashboardService;
    }

    /**
     * 获取inceptor数据表信息
     *
     * @return
     */
    @GetMapping("/inceptor/getTableFormatCount")
    public Response<List<Iterm<String, Integer>>> getTableFormatCount(@RequestParam("type") String type) {
        return new Response<List<Iterm<String, Integer>>>().setData(inceptorDashboardService.getTableFormatCount(type));
    }

    @GetMapping("/inceptor/getTableInfoDetail")
    public Response<Page<TableInfo>> getTableInfoDetail(@RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                                        @RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo) {
        Sort sort = Sort.by(Sort.Direction.DESC, "tableId");
        PageRequest pageable = PageRequest.of(pageNo, pageSize).withSort(sort);
        Page<TableInfo> tableInfoDetail = inceptorDashboardService.getTableInfoDetail(pageable);
        return new Response<Page<TableInfo>>().setData(tableInfoDetail).setSuccess(true);
    }

    @GetMapping("/inceptor/getViewInfoDetail")
    public Response<Page<ViewInfo>> getViewInfoDetail(@RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                                      @RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo) {
        Sort sort = Sort.by(Sort.Direction.DESC, "viewId");
        PageRequest pageable = PageRequest.of(pageNo, pageSize).withSort(sort);
        Page<ViewInfo> viewInfoDetail = inceptorDashboardService.getViewInfoDetail(pageable);
        return new Response<Page<ViewInfo>>().setData(viewInfoDetail).setSuccess(true);
    }
}
