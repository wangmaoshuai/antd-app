package com.transwarp.antdapp.controller;

import com.transwarp.antdapp.entity.Certificate;
import com.transwarp.antdapp.entity.LoginResult;
import com.transwarp.antdapp.entity.Notice;
import com.transwarp.antdapp.entity.User;
import com.transwarp.antdapp.entity.common.Pair;
import com.transwarp.antdapp.entity.common.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@Slf4j
@RestController
public class LoginController {
    @PostMapping("/login/account")
    public ResponseEntity<LoginResult> login(@RequestBody Certificate certificate) {
        log.info("the login info is {}", certificate);
        LoginResult result = new LoginResult();
        result.setStatus("ok");
        result.setCurrentAuthority("admin");
        result.setType("account");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/currentUser")
    public Response<User> getCurrentUser() {
        User.Geographic geographic = new User.Geographic();
        Pair city = new Pair().setKey("330100").setLabel("杭州市");
        Pair province = new Pair().setKey("330000").setLabel("浙江省");
        geographic.setCity(city);
        geographic.setProvince(province);
        User user = new User();
        user.setUserId("00000001");
        user.setName("wms");
        user.setAddress("山东济南");
        user.setAvatar("https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png");
        user.setEmail("email@gmail.com");
        user.setPhone("123456789011");
        user.setAccess("admin");
        user.setCountry("China");
        user.setGroup("星环－某某某事业群－某某平台部－某某技术部－UED");
        user.setGeographic(geographic);
        Pair tag = new Pair().setKey("0").setLabel("很有想法的");
        user.setNotifyCount(1);
        user.setTags(Collections.singletonList(tag));
        return new Response<User>().setData(user).setSuccess(true);
    }

    @GetMapping("/notices")
    public Response<List<Notice>> getNotices() {
        Notice notice = new Notice()
                .setAvatar("https://gw.alipayobjects.com/zos/rmsportal/fcHMVNCjPOsbUGdEduuv.jpeg")
                .setType("message")
                .setId("000000001")
                .setTitle("aa 回复了你")
                .setDatetime("2017-08-07")
                .setClickClose(true)
                .setDescription("这种模板用于提醒谁与你发生了互动，左侧放『谁』的头像");
        return new Response<List<Notice>>().setData(Collections.singletonList(notice)).setSuccess(true);
    }

}
