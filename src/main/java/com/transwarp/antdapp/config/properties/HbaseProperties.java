package com.transwarp.antdapp.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * @author wangm
 */
@Data
@ConfigurationProperties(prefix = "transwarp")
public class HbaseProperties {
    private Map<String, String> config;

}
