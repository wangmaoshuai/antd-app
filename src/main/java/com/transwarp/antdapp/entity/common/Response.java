package com.transwarp.antdapp.entity.common;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Response<T> {
    private T data;
    private Boolean success;
    private String error;
}
