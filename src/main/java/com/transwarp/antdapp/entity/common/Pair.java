package com.transwarp.antdapp.entity.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * key-label
 */
@Data
@Accessors(chain = true)
public class Pair {
    private String key;
    private String label;
}
