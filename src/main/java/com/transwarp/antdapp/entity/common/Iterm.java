package com.transwarp.antdapp.entity.common;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * @author wangm
 */
@Data
@Accessors(chain = true)
public class Iterm<K, V> {
    private K x;
    private V y;
}
