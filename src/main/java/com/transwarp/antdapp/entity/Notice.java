package com.transwarp.antdapp.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Notice {
    private String avatar;
    private String datetime;
    private String id;
    private String title;
    private String type;
    private Boolean clickClose;
    private String description;
}
