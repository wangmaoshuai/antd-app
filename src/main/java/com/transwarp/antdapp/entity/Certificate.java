package com.transwarp.antdapp.entity;

import lombok.Data;

/**
 * 登录凭证
 */
@Data
public class Certificate {
    private String username;
    private String password;
    private Boolean autoLogin;
    private String type;
}
