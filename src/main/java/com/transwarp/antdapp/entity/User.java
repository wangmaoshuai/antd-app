package com.transwarp.antdapp.entity;

import com.transwarp.antdapp.entity.common.Pair;
import lombok.Data;

import java.util.List;

/**
 * 用户信息
 */
@Data
public class User {
    private String name;
    private String avatar;
    private String userId;
    private String email;
    private String address;
    private String phone;
    private List<Pair> tags;
    private Geographic geographic;
    private Integer notifyCount;
    private Integer unreadCount;
    private String country;
    private String access;
    private String signature;
    private String title;
    private String group;

    @Data
    public static class Geographic {
        private Pair province;
        private Pair city;
    }
}
