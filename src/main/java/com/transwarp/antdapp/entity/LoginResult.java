package com.transwarp.antdapp.entity;

import lombok.Data;

/**
 * 登录结果
 */
@Data
public class LoginResult {
    private String status;
    private String type;
    private String currentAuthority;
}
