package com.transwarp.antdapp.entity.dashboard;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wangm
 */
@Data
@Accessors(chain = true)
public class Offline {
    private List<OfflineData> offlineData;
    private List<OfflineChart> offlineChart;

    @Data
    @Accessors(chain = true)
    private static class OfflineData {
        private String name;
        private Double cvr;
    }

    @Data
    @Accessors(chain = true)
    private static class OfflineChart {
        private String date;
        private String type;
        private Double value;
    }

    public static Offline getData() {
        List<OfflineData> offlineData = new ArrayList<>(10);
        int hbaseTableCount = 10;
        for (int i = 0; i < hbaseTableCount; i++) {
            offlineData.add(new OfflineData().setName(String.format("Hbase %d", i)).setCvr(
                    Math.ceil(Math.random() * 9) / 10
            ));
        }
        List<OfflineChart> offlineChartData = new ArrayList<>();
        int chartDataCount = 20;
        for (int i = 0; i < chartDataCount; i++) {
            long timestamp = System.currentTimeMillis() + 1000 * 60 * 30 * i;
            DateTimeFormatter pattern = DateTimeFormatter.ofPattern("HH:mm");
            String format = pattern.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault()));
            offlineChartData.add(new OfflineChart().setDate(format).setType("客流量").setValue(Math.floor(Math.random() * 100) + 10));
            offlineChartData.add(new OfflineChart().setDate(format).setType("支付笔数").setValue(Math.floor(Math.random() * 100) + 10));
        }

        return new Offline().setOfflineData(offlineData).setOfflineChart(offlineChartData);
    }
}
