package com.transwarp.antdapp.entity.dashboard;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangm
 */

@Data
@Accessors(chain = true)
public class ProportionSales {
    @Data
    private static class Item {
        private String x;
        private Integer y;
    }

    private static final Map<String, String> SALE_DATA = Collections.unmodifiableMap(new HashMap<String, String>(3) {{
        put("all", "[{\"x\":\"家用电器\",\"y\":4544},{\"x\":\"食用酒水\",\"y\":3321},{\"x\":\"个护健康\",\"y\":3113},{\"x\":\"服饰箱包\",\"y\":2341},{\"x\":\"母婴产品\",\"y\":1231},{\"x\":\"其他\",\"y\":1231}]");
        put("online", "[{\"x\":\"家用电器\",\"y\":244},{\"x\":\"食用酒水\",\"y\":321},{\"x\":\"个护健康\",\"y\":311},{\"x\":\"服饰箱包\",\"y\":41},{\"x\":\"母婴产品\",\"y\":121},{\"x\":\"其他\",\"y\":111}]");
        put("stores", "[{\"x\":\"家用电器\",\"y\":99},{\"x\":\"食用酒水\",\"y\":188},{\"x\":\"个护健康\",\"y\":344},{\"x\":\"服饰箱包\",\"y\":255},{\"x\":\"其他\",\"y\":65}]");
    }});

    public static JSONArray getMockData(String type) {
        return JSONObject.parseArray(SALE_DATA.get(type));
    }
}
