package com.transwarp.antdapp.entity.dashboard;

import com.github.javafaker.Faker;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author wangm
 */
@Data
@Accessors(chain = true)
public class Word {
    private List<MetaData> data;

    @Data
    @Accessors(chain = true)
    public static class MetaData {
        private String word;
        private Integer weight;
        private Integer id;
    }

    public static List<MetaData> getData() {
        Faker faker = new Faker(Locale.CHINA);
        int wordCount = 100;
        List<MetaData> result = new ArrayList<>(100);
        for (int i = 0; i < wordCount; i++) {
            MetaData m = new MetaData();
            m.setWord(faker.name().fullName());
            m.setId(i);
            m.setWeight((int) (Math.random() * 100 + 1));
            result.add(m);
        }
        return result;
    }
}
