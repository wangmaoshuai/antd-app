package com.transwarp.antdapp.service;

import com.transwarp.antdapp.entity.common.Iterm;
import com.transwarp.antdapp.repository.inceptor.bean.TableInfo;
import com.transwarp.antdapp.repository.inceptor.bean.ViewInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author wangm
 * 获取inceptor 表数据
 */
public interface InceptorDashboardService {
    /**
     * 获取inceptor数据库中表的分类统计
     * @return 统计信息
     * @param type
     */
    List<Iterm<String, Integer>> getTableFormatCount(String type);

    /**
     * 分页获取实际表的信息
     * @param pageable
     * @return
     */
    Page<TableInfo> getTableInfoDetail(Pageable pageable);

    /**
     * 获取视图的信息
     * @param pageable
     * @return
     */
    Page<ViewInfo> getViewInfoDetail(Pageable pageable);
}
