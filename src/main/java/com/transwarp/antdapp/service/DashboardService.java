package com.transwarp.antdapp.service;

import com.transwarp.antdapp.entity.common.Iterm;
import com.transwarp.antdapp.repository.es.bean.EsIndexInfo;
import com.transwarp.antdapp.repository.hbase.bean.HbaseTableInfo;

import java.util.List;

/**
 * @author wangm
 */
public interface DashboardService {
    /**
     * get all index
     *
     * @param type
     * @return
     */
    List<Iterm<String, Integer>> getIndexCount(String type);

    /**
     * 获取index详情列表
     * @return
     */
    List<EsIndexInfo> getIndexDetails();

    /**
     * 获取hbase的table元数据信息
     * @return
     */
    List<HbaseTableInfo> getHbaseTableDetails();
}
