package com.transwarp.antdapp.service.impl;

import com.google.gson.internal.Streams;
import com.transwarp.antdapp.entity.common.Iterm;
import com.transwarp.antdapp.repository.inceptor.ColumnsDetailRepository;
import com.transwarp.antdapp.repository.inceptor.InceptorRepository;
import com.transwarp.antdapp.repository.inceptor.TableDetailRepository;
import com.transwarp.antdapp.repository.inceptor.ViewDetailRepository;
import com.transwarp.antdapp.repository.inceptor.bean.TableColumn;
import com.transwarp.antdapp.repository.inceptor.bean.TableFormatCount;
import com.transwarp.antdapp.repository.inceptor.bean.TableInfo;
import com.transwarp.antdapp.repository.inceptor.bean.ViewInfo;
import com.transwarp.antdapp.service.InceptorDashboardService;
import com.transwarp.antdapp.util.InceptorUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author wangm
 */

@Service
public class InceptorDashboardServiceImpl implements InceptorDashboardService {
    private final InceptorRepository inceptorRepository;
    private final TableDetailRepository tableDetailRepository;
    private final ViewDetailRepository viewDetailRepository;
    private final ColumnsDetailRepository columnsDetailRepository;
    private final Map<String, Function<String, List<Iterm<String, Integer>>>> FORMAT_COUNT;
    private Map<String, List<TableColumn>> TABLE_COLUMN;

    public InceptorDashboardServiceImpl(InceptorRepository inceptorRepository, TableDetailRepository tableDetailRepository,
                                        ViewDetailRepository viewDetailRepository, ColumnsDetailRepository columnsDetailRepository) {
        this.inceptorRepository = inceptorRepository;
        this.tableDetailRepository = tableDetailRepository;
        this.viewDetailRepository = viewDetailRepository;
        this.columnsDetailRepository = columnsDetailRepository;
        FORMAT_COUNT = Collections
                .unmodifiableMap(new HashMap<String, Function<String, List<Iterm<String, Integer>>>>() {
                    {
                        put("all", (type) -> {
                            List<TableFormatCount> viewCount = inceptorRepository.getViewCount();
                            List<TableFormatCount> tableFormatCount = inceptorRepository.getTableFormatCount();
                            List<Iterm<String, Integer>> res = new ArrayList<>(2);
                            viewCount.forEach(x -> res.add(new Iterm<String, Integer>().setX(x.getType()).setY(x.getCount())));
                            tableFormatCount.forEach(x -> res.add(new Iterm<String, Integer>().setX(x.getType()).setY(x.getCount())));
                            return res;
                        });
                        put("cache", (type) -> inceptorRepository.getViewCount().stream()
                                .map(x -> new Iterm<String, Integer>().setX(x.getType()).setY(x.getCount())).collect(Collectors.toList()));
                        put("persistence", (type) -> inceptorRepository.getTableFormatCount().stream()
                                .map(x -> new Iterm<String, Integer>().setX(x.getType()).setY(x.getCount())).collect(Collectors.toList()));
                    }
                });
        TABLE_COLUMN = StreamSupport.stream(columnsDetailRepository.findAll().spliterator(), false).collect(Collectors.groupingBy((x) -> x.getTableName() + ":" + x.getDatabaseName()));
    }

    private synchronized void refreshTableColumn() {
        TABLE_COLUMN.clear();
        TABLE_COLUMN = StreamSupport.stream(columnsDetailRepository.findAll().spliterator(), false).collect(Collectors.groupingBy((x) -> x.getTableName() + ":" + x.getDatabaseName()));
    }

    @Override
    public List<Iterm<String, Integer>> getTableFormatCount(String type) {
        return FORMAT_COUNT.getOrDefault(type, (x) -> Collections.emptyList()).apply(type);
    }

    @Override
    public Page<TableInfo> getTableInfoDetail(Pageable pageable) {
        Page<TableInfo> all = tableDetailRepository.findAll(pageable);
        all.forEach(x -> {
            String key = x.getTableName() + ":" + x.getDatabaseName();
            if (!TABLE_COLUMN.containsKey(key)) {
                refreshTableColumn();
            }
            Double sum = TABLE_COLUMN.get(key).stream().map(column -> InceptorUtils.COLUMN_SCALE.getOrDefault(column.getColumnType(), 0.5)).reduce(Double::sum).orElse(1.0);
            x.setCompression(sum / TABLE_COLUMN.get(key).size()*100);
        });
        return all;
    }

    @Override
    public Page<ViewInfo> getViewInfoDetail(Pageable pageable) {
        return viewDetailRepository.findAll(pageable);
    }
}
