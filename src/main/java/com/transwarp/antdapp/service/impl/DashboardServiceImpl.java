package com.transwarp.antdapp.service.impl;

import com.transwarp.antdapp.entity.common.Iterm;
import com.transwarp.antdapp.repository.ElasticRepository;
import com.transwarp.antdapp.repository.HbaseRepository;
import com.transwarp.antdapp.repository.es.bean.EsIndexInfo;
import com.transwarp.antdapp.repository.hbase.bean.HbaseTableInfo;
import com.transwarp.antdapp.service.DashboardService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;

/**
 * @author wangm
 */
@Service
public class DashboardServiceImpl implements DashboardService {
    private final ElasticRepository elasticRepository;
    private final HbaseRepository hbaseRepository;
    private final Map<String, Function<String, List<Iterm<String, Integer>>>> INDEX_COUNT;

    public DashboardServiceImpl(ElasticRepository elasticRepository, HbaseRepository hbaseRepository) {
        this.elasticRepository = elasticRepository;
        this.hbaseRepository = hbaseRepository;
        INDEX_COUNT = Collections.unmodifiableMap(new HashMap<String, Function<String, List<Iterm<String, Integer>>>>(3) {
            {
                put("all", (type) -> {
                    Integer tableCount = hbaseRepository.getTableCount();
                    Integer indexCount = elasticRepository.getIndexCount();
                    List<Iterm<String, Integer>> res = new ArrayList<>(2);
                    res.add(new Iterm<String, Integer>().setX("Hbase").setY(tableCount));
                    res.add(new Iterm<String, Integer>().setX("Elasticsearch").setY(indexCount));
                    return res;
                });
                put("secondIndex", (type) -> {
                    Integer tableCount = hbaseRepository.getTableCount();
                    return Collections.singletonList(new Iterm<String, Integer>().setX("Hbase").setY(tableCount));
                });
                put("fullTextIndex", (type) -> {
                    Integer indexCount = elasticRepository.getIndexCount();
                    return Collections.singletonList(new Iterm<String, Integer>().setX("Elasticsearch").setY(indexCount));
                });
            }
        });
    }

    @Override
    public List<Iterm<String, Integer>> getIndexCount(String type) {
        return INDEX_COUNT.getOrDefault(type, (x) -> Collections.emptyList()).apply(type);
    }

    @Override
    public List<EsIndexInfo> getIndexDetails() {
        return elasticRepository.getIndexDetails();
    }

    @Override
    public List<HbaseTableInfo> getHbaseTableDetails() {
        return hbaseRepository.getTableInfoDetails();
    }
}
