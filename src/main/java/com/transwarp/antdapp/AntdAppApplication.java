package com.transwarp.antdapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangm
 */
@SpringBootApplication()
public class AntdAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AntdAppApplication.class, args);
    }

}
