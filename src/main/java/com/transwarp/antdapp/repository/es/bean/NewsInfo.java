package com.transwarp.antdapp.repository.es.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * @author wangm
 */
@Data
@AllArgsConstructor
@Accessors(chain = true)
public class NewsInfo {
    private String id;
    private String title;
    private Object content;

}
