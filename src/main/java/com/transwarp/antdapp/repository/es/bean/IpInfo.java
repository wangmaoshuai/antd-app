package com.transwarp.antdapp.repository.es.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class IpInfo {
    private String id;
    private String ip;
    private String status;
    @JSONField (format="yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    @JSONField (format="yyyy-MM-dd HH:mm:ss")
    private Date  updatedAt;
    private List<Label> labels;

    @Data
    @Accessors(chain = true)
    private static class Label {
        private String color;
        private String name;
    }
}
