package com.transwarp.antdapp.repository.es.bean;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wangm
 */
@Data
@Accessors(chain = true)
public class EsIndexInfo {
    private String health;
    private String status;
    private String index;
    @JSONField(name="uuid")
    private String id;
    @JSONField(name="docs.count")
    private String docCount;
    @JSONField(name = "store.size")
    private String storeSize;
}
