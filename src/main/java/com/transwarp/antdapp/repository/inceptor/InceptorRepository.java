package com.transwarp.antdapp.repository.inceptor;

import com.transwarp.antdapp.repository.inceptor.bean.TableFormatCount;
import com.transwarp.antdapp.repository.inceptor.bean.TableInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wangm
 */
@Repository
@RequiredArgsConstructor
public class InceptorRepository {
    private final JdbcTemplate jdbcTemplate;

    public List<TableInfo> getMetaData() {
        return jdbcTemplate.query("select * from system.tables_v;", BeanPropertyRowMapper.newInstance(TableInfo.class));
    }

    public List<TableFormatCount> getTableFormatCount() {
        return jdbcTemplate.query("select table_format as type,count(table_id) as count from system.tables_v group by table_format order by count desc;",
                BeanPropertyRowMapper.newInstance(TableFormatCount.class));
    }

    public List<TableFormatCount> getViewCount() {
        return jdbcTemplate.query("select count(view_id) as count, '结果集缓存表' as type from system.views_v"
                , BeanPropertyRowMapper.newInstance(TableFormatCount.class));
    }

}
