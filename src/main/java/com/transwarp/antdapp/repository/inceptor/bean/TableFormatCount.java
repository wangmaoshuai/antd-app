package com.transwarp.antdapp.repository.inceptor.bean;

import lombok.Data;

import java.util.Optional;

/**
 * @author wangm
 */
@Data
public class TableFormatCount {
    private String type;
    private Integer count;

    public String getType() {
        String[] typeArray = type.split("\\.");
        return Optional.ofNullable(typeArray[typeArray.length-1]).orElse(null);
    }
}
