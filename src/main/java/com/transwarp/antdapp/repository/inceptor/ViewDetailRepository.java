package com.transwarp.antdapp.repository.inceptor;

import com.transwarp.antdapp.repository.inceptor.bean.ViewInfo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author wangm
 */
@Repository
public interface ViewDetailRepository extends PagingAndSortingRepository<ViewInfo, Integer> {
}
