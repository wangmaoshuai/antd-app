package com.transwarp.antdapp.repository.inceptor.bean;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Date;

/**
 * @author wangm
 */
@Data
@Table("tables_v")
public class TableInfo {
    @Id
    private Integer tableId;
    private String tableName;
    private String databaseName;
    private Date createTime;
    private String tableType;
    private String ownerName;
    @Column("commentstring")
    private String commentString;
    private String transactional;
    private Date lastLoadTime;
    private String inputFormat;
    private String tableFormat;
    private String tableLocation;
    private String rowPermission;
    private String columnPermission;
    private String hbaseName;
    private String fieldDelim;
    private String lineDelim;
    private String collectionDelim;
    /**
     * 压缩率
     */
    @Transient
    private Double compression;
}
