package com.transwarp.antdapp.repository.inceptor;

import com.transwarp.antdapp.repository.inceptor.bean.TableInfo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author wangm
 * 数据表信息详情
 */
@Repository
public interface TableDetailRepository extends PagingAndSortingRepository<TableInfo, Integer> {
}
