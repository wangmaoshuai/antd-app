package com.transwarp.antdapp.repository.inceptor;

import com.transwarp.antdapp.repository.inceptor.bean.TableColumn;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author wangm
 */

@Repository
public interface ColumnsDetailRepository extends PagingAndSortingRepository<TableColumn, Integer> {
}
