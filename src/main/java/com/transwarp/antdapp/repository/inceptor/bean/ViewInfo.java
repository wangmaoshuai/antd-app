package com.transwarp.antdapp.repository.inceptor.bean;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Date;

/**
 * @author wangm
 */
@Data
@Table("views_v")
public class ViewInfo {
    @Id
    private Integer viewId;
    private String viewName;
    private String databaseName;
    private Date  createTime;
    private String originText;
    private String ownerName;
}
