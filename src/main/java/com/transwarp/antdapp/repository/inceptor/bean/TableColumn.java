package com.transwarp.antdapp.repository.inceptor.bean;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 * @author wangm
 */
@Data
@Table("columns_v")
public class TableColumn {
    @Id
    private Integer columnId;
    private String columnName;
    private String columnType;
    private String tableName;
    private String databaseName;
    private Integer columnScale;
    private Integer columnLength;
}
