package com.transwarp.antdapp.repository.hbase.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wangm
 */
@Data
@Accessors(chain = true)
public class HbaseTableInfo {
    private String tableName;
    private String tableMetadata;
    private boolean isSystemTable;
}
