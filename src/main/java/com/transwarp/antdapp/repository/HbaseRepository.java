package com.transwarp.antdapp.repository;

import com.transwarp.antdapp.repository.hbase.bean.HbaseTableInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author wangm
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class HbaseRepository {
    private final HbaseTemplate hbaseTemplate;

    public List<String> getTableNames() {
        try {
            Connection connection = getConnection();
            Admin admin = connection.getAdmin();
            TableName[] tableNames = admin.listTableNames();
            return Arrays.stream(tableNames).map(TableName::toString).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("get hbase table error", e);
            return null;
        }
    }

    public Integer getTableCount() {
        try {
            Connection conf = getConnection();
            Admin admin = conf.getAdmin();
            TableName[] tableNames = admin.listTableNames();
            return tableNames.length;
        } catch (Exception e) {
            log.error("get hbase table error", e);
            return 0;
        }
    }

    public Connection getConnection() throws Exception {
        Configuration conf = hbaseTemplate.getConfiguration();
        Connection connection;
        try {
            connection = ConnectionFactory.createConnection(conf);
            return connection;
        } catch (Exception e) {
            log.error("get connection error", e);
            throw e;
        }
    }

    public List<HbaseTableInfo> getTableInfoDetails() {
        try {
            Connection connection = getConnection();
            Admin admin = connection.getAdmin();
            TableName[] tableNames = admin.listTableNames();
            List<HbaseTableInfo> result = new ArrayList<>(tableNames.length);
            Arrays.stream(tableNames).forEach(name -> {
                try {
                    result.add(new HbaseTableInfo()
                            .setTableName(name.getNameAsString())
                            .setTableMetadata(admin.getTableDescriptor(name).toString())
                            .setSystemTable(name.isSystemTable()));
                } catch (IOException e) {
                    log.error("get hbase table metadata error", e);
                }
            });
            return result;
        } catch (Exception e) {
            log.error("get hbase table error", e);
            return null;
        }
    }
}
