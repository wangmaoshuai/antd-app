package com.transwarp.antdapp.repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.transwarp.antdapp.repository.es.bean.EsIndexInfo;
import com.transwarp.antdapp.repository.es.bean.IpInfo;
import com.transwarp.antdapp.repository.es.bean.NewsInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author wangm
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class ElasticRepository {
    private final RestHighLevelClient restHighLevelClient;
    private final RestClient restClient;

    public String getEsInfo() {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // SearchRequest
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        try {
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest);
            return searchResponse.toString();
        } catch (IOException e) {
            log.error("search info error", e);
            return null;
        }
    }

    public Integer getIndexCount() {
        try {
            Response response = restClient.performRequest("GET", "/_cat/indices?v&format=json");
            if (response != null) {
                String rawBody = EntityUtils.toString(response.getEntity());
                JSONArray jsonArray = JSON.parseArray(rawBody);
                return jsonArray.size();
            }
        } catch (Exception e) {
            log.error("get index count error", e);
        }
        return 0;
    }

    public List<EsIndexInfo> getIndexDetails() {
        try {
            Response response = restClient.performRequest("GET", "/_cat/indices?v&format=json");
            if (response != null) {
                String rawBody = EntityUtils.toString(response.getEntity());
                return JSONArray.parseArray(rawBody, EsIndexInfo.class);
            }
        } catch (Exception e) {
            log.error("get index detail error", e);
        }
        return Collections.emptyList();
    }

    public List<NewsInfo> searchNews(String index, Integer from, Integer size) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        sourceBuilder.from(from);
        sourceBuilder.size(size);
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        return getNewsInfos(index, sourceBuilder);
    }

    public List<NewsInfo> searchNews(String searchKey, String index, Integer from, Integer size) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        sourceBuilder.from(from);
        sourceBuilder.size(size);
        sourceBuilder.query(QueryBuilders.matchQuery("content", searchKey));
        return getNewsInfos(index, sourceBuilder);
    }

    private List<NewsInfo> getNewsInfos(String index, SearchSourceBuilder sourceBuilder) {
        SearchRequest searchRequest = new SearchRequest(index).source(sourceBuilder);
        try {
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest);
            SearchHits hits = searchResponse.getHits();
            List<NewsInfo> res = new ArrayList<>(hits.getHits().length);
            hits.forEach(x -> res.add(new NewsInfo(x.getId(),
                    x.getSource().getOrDefault("title", "无").toString(),
                    x.getSource().getOrDefault("content", ""))));
            return res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Page<IpInfo> searchIpInfo(String ip, String status, String startTime, String endTime, String index, Integer from, Integer size) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        sourceBuilder.from(from);
        sourceBuilder.size(size);
        if (!StringUtils.isEmpty(ip) || !StringUtils.isEmpty(status) || !StringUtils.isEmpty(startTime) || !StringUtils.isEmpty(endTime)) {
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
            if (!StringUtils.isEmpty(ip)) {
                boolQueryBuilder.must(QueryBuilders.termQuery("ip", ip));
            }
            if (!StringUtils.isEmpty(status)) {
                boolQueryBuilder.must(QueryBuilders.termQuery("status", status));
            }
            if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
                boolQueryBuilder.must(QueryBuilders.rangeQuery("created_at").gte(startTime).lte(endTime).format("yyyy-MM-dd"));
            }
            sourceBuilder.query(boolQueryBuilder);
        } else {
            sourceBuilder.query(QueryBuilders.matchAllQuery());
        }
        PageRequest pageRequest = PageRequest.of(from, size);
        return getIpInfo(index, sourceBuilder, pageRequest);
    }

    private Page<IpInfo> getIpInfo(String index, SearchSourceBuilder sourceBuilder, Pageable pageable) {
        SearchRequest searchRequest = new SearchRequest(index).source(sourceBuilder);
        DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest);
            SearchHits hits = searchResponse.getHits();
            long totalHits = searchResponse.getHits().totalHits;
            List<IpInfo> res = new ArrayList<>(hits.getHits().length);
            hits.forEach(x -> {
                IpInfo ipInfo = JSON.toJavaObject(JSONObject.parseObject(x.getSourceAsString()), IpInfo.class);
                ipInfo.setId(x.getId());
                res.add(ipInfo);
            });
            return new PageImpl<>(res, pageable, totalHits);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
