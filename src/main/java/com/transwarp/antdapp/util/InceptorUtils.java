package com.transwarp.antdapp.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangm
 */
public final class InceptorUtils {
    private InceptorUtils(){}

    /**
     * 压缩率 造的数据
     */
    public static final Map<String,Double> COLUMN_SCALE= Collections.unmodifiableMap(new HashMap<String,Double>(35){
        {
            put("map<string,string>",0.8);
            put("decimal(6,1)",0.6);
            put("tinyint",0.1);
            put("bigint",0.3);
            put("varchar(10)",0.4);
            put("varchar2(767,ORACLE)",0.6);
            put("decimal(10,2)",0.7);
            put("int",0.2);
            put("array<double>",0.4);
            put("varchar2(255,ORACLE)",0.5);
            put("array<string>",0.7);
            put("boolean",0.1);
            put("float",0.4);
            put("varchar2(128,ORACLE)",0.5);
            put("string",1.0);
            put("decimal(28,2)",0.5);
            put("double",0.5);
            put("varchar2(10,TD)",0.6);
            put("varchar(64)",0.5);
            put("decimal(18,6)",0.4);
            put("varchar2(4000,ORACLE)",0.7);
            put("varchar2(10,ORACLE)",0.6);
            put("varchar2(256,ORACLE)",0.8);
            put("decimal(8,0)",0.3);
            put("decimal(18,2)",0.4);
            put("decimal(28,6)",0.5);
            put("date",1.0);
            put("binary",0.5);
            put("varchar(100)",0.6);
            put("varchar2(20,ORACLE)",0.7);
            put("decimal(38,0)",0.3);
            put("timestamp",1.0);
            put("varchar2(74,ORACLE)",0.4);
            put("varchar2(127,ORACLE)",0.5);
            put("varchar2(20,TD)",0.9);
        }
    });
}
